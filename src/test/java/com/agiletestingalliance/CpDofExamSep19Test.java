package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import org.junit.Test;
import com.agiletestingalliance.AboutCPDOF;
import com.agiletestingalliance.Duration;
import com.agiletestingalliance.MinMax;
import com.agiletestingalliance.Usefulness;


public class CpDofExamSep19Test {

    @Test
    public void aboutCPDOFTest() throws Exception {
        String result = new AboutCPDOF().desc();
		String localStr = "CP-DOF certification program covers end to end DevOps Life Cycle practically. CP-DOF is the only globally recognized certification program which has the following key advantages: <br> 1. Completely hands on. <br> 2. 100% Lab/Tools Driven <br> 3. Covers all the tools in entire lifecycle <br> 4. You will not only learn but experience the entire DevOps lifecycle. <br> 5. Practical Assessment to help you solidify your learnings.";
		assertEquals("String check failed AboutCPDOF Desc",localStr,result);
    }

	@Test
    public void durationTest() throws Exception {
        String result = new Duration().dur();
		String localStr = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
		assertEquals("String check failed Duration Dur",localStr,result);
    }
	
    @Test
    public void minMaxTest() throws Exception {

        int k= new MinMax().testFunction(9,10);
        assertEquals("Addition check failed",k,10);
		
		int l= new MinMax().testFunction(3,2);
        assertEquals("Addition check failed",l,3);
        
    }

	@Test
    public void usefulnessTest() throws Exception {
        String result = new Usefulness().desc();
		String localStr = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";
		assertEquals("String check failed Usefulness Desc",localStr,result);
    }


}
